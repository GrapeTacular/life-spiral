import React, { useEffect, useRef, Suspense } from 'react';
import { Canvas, useThree, useFrame } from '@react-three/fiber';
import { FlyControls } from '@react-three/drei';
import { CubeTextureLoader } from "three";


// App

function App() {
  const {
    interval,
    birth,
    survival,
    circumphrence,
    height
  } = getUrlParameters();

  const lifeRuleFunc = getLifeRuleFunc(birth, survival);

  return (
    <div className="environment">
      <Canvas>
        <Suspense fallback={null}>
          <ambientLight intensity={1} />
          <BoxGrid
            interval={interval}
            scale={0.5}
            lifeRuleFunc={lifeRuleFunc}
            circumphrence={circumphrence}
            height={height} />
          <SkyBox />
        </Suspense>
        <FlyControls
          dragToLook={true}
          autoForward={false}
          rollSpeed={0.5}
        />
      </Canvas>
    </div>
  );
}

export default App;


// JSX Elements

function BoxGrid({ interval, scale, lifeRuleFunc, circumphrence, height }) {
  const refState = useRef(getInitialGridState(circumphrence, height));
  const refCoordinates = useRef(getGridCoordinates(circumphrence, height));
  const refElements = useRef([]);

  useEffect(() => {
    refElements.current = refElements.current.slice(0, refState.current.length);
  }, []);

  function applyNextGridStateToElements(nextGridState) {
    for (let i = 0; i < refElements.current.length; ++i) {
      const { counter, alive, color } = nextGridState[i];

      refState.current[i].counter = counter;
      refState.current[i].alive = alive;

      const { r, g, b } = color;

      refElements.current[i].material.color.r = r;
      refElements.current[i].material.color.g = g;
      refElements.current[i].material.color.b = b;
    }
  }

  useFrame(() => {
    const state = refState.current;
    const coordinates = refCoordinates.current;
    const nextGridState = getNextGridState(state, coordinates, interval, lifeRuleFunc, circumphrence, height);
    applyNextGridStateToElements(nextGridState);
  });

  return (
    <>
      {refState.current.map((s, i) => {
        return (
          <mesh
            key={i}
            position={refCoordinates.current[i].position}
            rotation={refCoordinates.current[i].rotation}
            scale={scale}
            ref={element => (refElements.current[i] = element)}
          >
            <boxGeometry
              attach="geometry"
              args={[1, 1, 1]}
            />
            <meshStandardMaterial
              displacementScale={0.2}
              color={s.color}
            />
          </mesh>
        );
      })}
    </>
  );
}

function SkyBox() {
  const { scene } = useThree();
  const loader = new CubeTextureLoader();

  const texture = loader.load([
    'xpos.png',
    'xneg.png',
    'ypos.png',
    'yneg.png',
    'zpos.png',
    'zneg.png'
  ]);

  scene.background = texture;
  return null;
}


// helpers

function getUrlParameters() {
  const queryString = window.location.search;
  const urlParameters = new URLSearchParams(queryString);

  const interval = urlParameters.get('interval') ? urlParameters.get('interval') : '10';
  const birth = urlParameters.get('birth') ? urlParameters.get('birth') : '3';
  const survival = urlParameters.get('survival') ? urlParameters.get('survival') : '23';
  const circumphrence = urlParameters.get('circumphrence') ? urlParameters.get('circumphrence') : '30';
  const height = urlParameters.get('height') ? urlParameters.get('height') : '20';

  window.history.replaceState(
    {},
    '',
    `?interval=${interval}&birth=${birth}&survival=${survival}&circumphrence=${circumphrence}&height=${height}`
  );

  return {
    interval: parseInt(interval, 10),
    birth: parseInt(birth, 10),
    survival: parseInt(survival, 10),
    circumphrence: parseInt(circumphrence, 10),
    height: parseInt(height, 10)
  };
}

function getLifeRuleFunc(birth, survival) {
  const b = [];
  const s = [];

  while (birth > 0) {
    b.push(birth % 10);
    birth = Math.floor(birth / 10);
  }

  while (survival > 0) {
    s.push(survival % 10);
    survival = Math.floor(survival / 10);
  }

  return (alive, count) => alive ? s.includes(count) : b.includes(count);
}

const Top = 255;

const Colors = [
  { r: 37 / Top, g: 150 / Top, b: 190 / Top },
  { r: 54 / Top, g: 94 / Top, b: 62 / Top }
];

function getColorAtIndex(i) {
  return Colors[i];
}

function getRGBColor({ r, g, b }) {
  return `rgb(${Math.floor(r * Top)}, ${Math.floor(g * Top)}, ${Math.floor(b * Top)})`;
}

function getRGBColorAtIndex(i) {
  const color = getColorAtIndex(i);
  return getRGBColor(color);
}

const PI = 3.14159;

function getInitialGridState(circumphrence, height) {
  function getGrid() {
    let grid = [];
    for (let h = 0; h < height; ++h) {
      for (let c = 0; c < circumphrence; ++c) {
        const counter = 0;
        const alive = Math.random() < 0.5;
        const color = alive ? getRGBColorAtIndex(0) : getRGBColorAtIndex(1);
        grid.push({ counter, alive, color });
      }
    }
  
    return grid;
  }

  return getGrid();
}

function getGridCoordinates(circumphrence, height) {
  const r = circumphrence / (PI * 2);

  function getInitialPosition(h, c) {
    const a = 2 * PI * c / circumphrence;
    const x = r * Math.cos(a);
    const z = r * Math.sin(a);
    return [x, h - 0.5 * height, z + r];
  }

  function getInitialRotation(c) {
    const a = -2 * PI * c / circumphrence;
    return [0, a, 0];
  }

  function getGrid() {
    let grid = [];
    for (let h = 0; h < height; ++h) {
      for (let c = 0; c < circumphrence; ++c) {
        const position = getInitialPosition(h, c);
        const rotation = getInitialRotation(c);
        grid.push({ coordinate: { h, c }, position, rotation });
      }
    }
  
    return grid;
  }

  return getGrid();
}

function getNextGridState(gridState, gridCoordinates, interval, lifeRuleFunc, circumphrence, height) {
  function getIndexFromCoordinate(h, c) {
    return h * circumphrence + c;
  }

  function getSurroundingCoordinates(index) {
    const { h, c } = gridCoordinates[index].coordinate;

    const coordinates = [];
    for (let hi = h - 1; hi <= h + 1; ++hi) {
      for (let ci = c - 1; ci <= c + 1; ++ci) {
        if (hi === h && ci === c) continue;

        let hj = hi;
        let cj = ci;

        if (hj < 0)
          hj = height - 1;
        else if (hj >= height)
          hj = 0;

        if (cj < 0)
          cj = circumphrence - 1;
        else if (cj >= circumphrence)
          cj = 0;

        coordinates.push({ h: hj, c: cj });
      }
    }

    return coordinates;
  }

  function getNextLifeState(alive, index) {
    const coordinates = getSurroundingCoordinates(index);
    
    const count = coordinates.reduce((accum, coordinate) => {
      const { h, c } = coordinate;
      const i = getIndexFromCoordinate(h, c);
      return accum + (gridState[i].alive ? 1 : 0);
    }, 0);

    return lifeRuleFunc(alive, count);
  }

  function getNextState(index) {
    const { counter, alive } = gridState[index];

    const nextCounter = counter < interval
      ? counter + 1
      : 0;
    const nextAlive = nextCounter === 0
      ? getNextLifeState(alive, index)
      : alive;
    const nextColor = alive ? getColorAtIndex(0) : getColorAtIndex(1);

    const result = {
      counter: nextCounter,
      alive: nextAlive,
      color: nextColor
    };

    return result;
  }

  function getNextGridState() {
    let nextGridState = [];
    for (let i = 0; i < gridState.length; ++i) {
      nextGridState.push(getNextState(i));
    }

    return nextGridState;
  }

  return getNextGridState();
}
